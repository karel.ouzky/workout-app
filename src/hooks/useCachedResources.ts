import { initWorkouts } from "./../storage/workout";
import { useEffect, useState } from "react";
import * as Font from "expo-font";

function useCachedResources() {
  const [isLoadiongComplete, setIsLoadingComplete] = useState(false);

  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        await initWorkouts();
        await Font.loadAsync({
          montserrat: require("../assets/fonts/Montserrat-Regular.ttf"),
          "montserrat-bold": require("../assets/fonts/Montserrat-Bold.ttf"),
        });
      } catch (e) {
        console.warn(e);
      } finally {
        setIsLoadingComplete(true);
      }
    }
    loadResourcesAndDataAsync();
  }, []);

  console.log("Returning:", isLoadiongComplete);
  return isLoadiongComplete;
}

export default useCachedResources;

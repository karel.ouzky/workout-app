import { useColorScheme } from "react-native";
import { PressebleText, PressebleTextProps } from "../PressebleText";

export function PressebleThemeText(props: PressebleTextProps) {
  const colorScheme = useColorScheme();
  const color = colorScheme === "light" ? "#000" : "#fff";

  return <PressebleText {...props} style={[props.style, { color }]} />;
}

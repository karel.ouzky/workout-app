import { Text } from "react-native";

export function MonteserratText(props: Text["props"]) {
  return (
    <Text {...props} style={[props.style, { fontFamily: "montserrat" }]} />
  );
}

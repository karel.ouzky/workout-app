import { Text, StyleSheet } from "react-native";

export function MonserratTextBold({ children }: { children: any }) {
  return <Text style={[styles.container]}>{children}</Text>;
}

const styles = StyleSheet.create({
  container: {
    fontFamily: "montserrat-bold",
    fontSize: 30,
  },
});

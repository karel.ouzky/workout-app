import { FunctionComponent, useState } from "react";
import { View, StyleSheet, Text, Modal as DefaultModal } from "react-native";
import { PressebleText } from "../PressebleText";

type ModalProps = {
  activator?: FunctionComponent<{
    handleOpen: () => void;
  }>;
  children?: FunctionComponent<{
    handleOpen: () => void;
    handleClose: () => void;
  }>;
};

export function Modal({ activator: Activator, children }: ModalProps) {
  const [isModalVisible, setModalVisible] = useState(false);

  const handleOpen = () => setModalVisible(true);
  const handleClose = () => setModalVisible(false);

  return (
    <View>
      <DefaultModal
        visible={isModalVisible}
        transparent={false}
        animationType={"fade"}
      >
        <View style={styles.centerView}>
          <View style={styles.contentView}>
            {children && children({ handleOpen, handleClose })}
          </View>
          <PressebleText text="Close" onPress={handleClose} />
        </View>
      </DefaultModal>
      {Activator ? (
        <Activator handleOpen={handleOpen} />
      ) : (
        <PressebleText text="Open" onPress={handleOpen} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contentView: {
    marginBottom: 20,
  },
});

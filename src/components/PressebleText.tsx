import {
  Text,
  Pressable,
  PressableProps,
  StyleProp,
  TextStyle,
} from "react-native";

export type PressebleTextProps = PressableProps & {
  text: string;
  style?: StyleProp<TextStyle>;
};

export function PressebleText(props: PressebleTextProps) {
  return (
    <Pressable {...props}>
      <Text style={[props.style, { textDecorationLine: "underline" }]}>
        {props.text}
      </Text>
    </Pressable>
  );
}

import { useState } from "react";
import { View, StyleSheet, TextInput, Button } from "react-native";
import { PressebleText } from "./PressebleText";
import { useForm, Controller } from "react-hook-form";
import { PressebleThemeText } from "./styled/PressebleThemeText";

export type ExcerciseFormData = {
  name: string;
  duration: string;
  type: string;
  reps?: string;
};

type WorkoutProps = {
  onSubmit: (form: ExcerciseFormData) => void;
};

const selectionItems = ["excercise", "break", "stretch"];

export default function ExcerciseForm({ onSubmit }: WorkoutProps) {
  const { control, handleSubmit } = useForm();
  const [isSelectionOn, setSelectionOn] = useState(false);

  return (
    <View>
      <View style={[styles.container]}></View>
      <View>
        <View style={styles.rowContainer}>
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            name="name"
            render={({ field: { onChange, value } }) => (
              <TextInput
                onChangeText={onChange}
                value={value}
                style={styles.input}
                placeholder="Name"
              />
            )}
          />
          <Controller
            control={control}
            rules={{
              required: false,
            }}
            name="duration"
            render={({ field: { onChange, value } }) => (
              <TextInput
                onChangeText={onChange}
                value={value}
                style={styles.input}
                placeholder="Duration"
                placeholderTextColor={"rgba(0,0,0,0.4)"}
              />
            )}
          />
        </View>
        <View style={styles.rowContainer}>
          <Controller
            control={control}
            rules={{
              required: false,
            }}
            name="reps"
            render={({ field: { onChange, value } }) => (
              <TextInput
                onChangeText={onChange}
                value={value}
                style={styles.input}
                placeholder="Repetitions"
                placeholderTextColor={"rgba(0,0,0,0.4)"}
              />
            )}
          />
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            name="type"
            render={({ field: { onChange, value } }) => (
              <View style={{ flex: 1 }}>
                {isSelectionOn ? (
                  <View>
                    {selectionItems.map((selection) => (
                      <PressebleText
                        key={selection}
                        style={styles.selection}
                        text={selection}
                        onPressIn={() => {
                          onChange(selection);
                          setSelectionOn(false);
                        }}
                      />
                    ))}
                  </View>
                ) : (
                  <TextInput
                    onPressIn={() => setSelectionOn(true)}
                    style={styles.input}
                    placeholder="Type"
                    value={value}
                    placeholderTextColor={"rgba(0,0,0,0.4)"}
                  />
                )}
              </View>
            )}
          />
        </View>

        <PressebleThemeText
          style={{ marginTop: 10 }}
          text="Add Excercise"
          onPress={handleSubmit((data) => {
            onSubmit(data as ExcerciseFormData);
          })}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "#fff",
    borderRadius: 10,
  },
  input: {
    margin: 2,
    borderWidth: 1,
    height: 30,
    padding: 5,
    borderRadius: 5,
    borderColor: "rgba(0,0,0,0.4)",
    flex: 1,
  },
  rowContainer: {
    flexDirection: "row",
    backgroundColor: "white",
  },
  selection: {
    margin: 2,
    padding: 3,
    alignSelf: "center",
  },
});

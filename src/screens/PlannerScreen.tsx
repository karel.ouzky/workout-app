import { useEffect, useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { StackHeaderProps } from "@react-navigation/stack";
import ExcerciseForm, { ExcerciseFormData } from "../components/ExcerciseForm";
import { SequenceItem, SequenceType, Workout } from "../types/data";
import slugify from "slugify";
import { FlatList } from "react-native-gesture-handler";
import ExcerciseItem from "../components/styled/ExcerciseItem";
import { PressebleText } from "../components/PressebleText";
import { Modal } from "../components/styled/Modal";
import WorkoutForm, { WorkoutFormData } from "../components/WorkoutForm";
import { storeWorkout } from "../storage/workout";
import { PressebleThemeText } from "../components/styled/PressebleThemeText";

function PlannerScreen({ navigation }: StackHeaderProps) {
  const [seqItems, setSeqItems] = useState<SequenceItem[]>([]);

  const handleExcerciseSubmit = (form: ExcerciseFormData) => {
    // alert(`${form.name} - ${form.duration} - ${form.reps} - ${form.type}`);
    const sequenceItem: SequenceItem = {
      slug: slugify(form.name + Date.now(), { lower: true }),
      name: form.name,
      type: form.type as SequenceType,
      duration: Number(form.duration),
    };
    if (form.reps) {
      sequenceItem.reps = Number(form.reps);
    }

    // console.log(sequenceItem);
    setSeqItems([...seqItems, sequenceItem]);
  };

  const computeDiff = (excecisesCount: number, workoutDuration: number) => {
    const intensity = workoutDuration / excecisesCount;

    if (intensity < 60) {
      return "hard";
    } else if (intensity <= 100) {
      return "medium";
    }
    return "easy";
  };

  const handleWorkoutSubmit = async (form: WorkoutFormData) => {
    if (seqItems.length > 0) {
      const duration = seqItems.reduce((acc, item) => {
        return acc + item.duration;
      }, 0);

      const workout: Workout = {
        name: form.name,
        slug: slugify(form.name + Date.now(), { lower: true }),
        difficulty: computeDiff(seqItems.length, duration),
        sequence: [...seqItems],
        duration,
      };

      console.log(workout);
      await storeWorkout(workout);
    }
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={seqItems}
        keyExtractor={(item) => item.slug}
        renderItem={({ item, index }) => (
          <ExcerciseItem item={item}>
            <PressebleText
              text="Remove"
              onPressIn={() => {
                const items = [...seqItems];
                items.splice(index, 1);
                setSeqItems(items);
              }}
            />
          </ExcerciseItem>
        )}
      />

      <ExcerciseForm onSubmit={handleExcerciseSubmit} />

      <View>
        <Modal
          activator={({ handleOpen }) => (
            <PressebleThemeText
              text="Create Workout"
              onPress={handleOpen}
              style={{ marginTop: 15 }}
            />
          )}
        >
          {({ handleClose }) => (
            <View>
              {/* <WorkoutForm onSubmit={handleWorkoutSubmit} /> */}
              <WorkoutForm
                onSubmit={async (data) => {
                  console.log("hello workd");
                  await handleWorkoutSubmit(data);
                  handleClose();

                  navigation.navigate("Home");
                }}
              />
            </View>
          )}
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default PlannerScreen;

export type Difficulty = "easy" | "medium" | "hard";
export type SequenceType = "excercise" | "break" | "stretch";

export interface SequenceItem {
  slug: string;
  name: string;
  type: SequenceType;
  reps?: number;
  duration: number;
}

export interface Workout {
  slug: string;
  name: string;
  duration: number;
  difficulty: Difficulty;
  sequence: SequenceItem[];
  reps?: number;
}

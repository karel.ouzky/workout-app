import { StatusBar } from "expo-status-bar";
import useCachedResources from "./src/hooks/useCachedResources";
import Navigation from "./src/navigation";
import { Text, useColorScheme } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";

export default function App() {
  const isLoaded = useCachedResources(); // the app gets to reexcecuted once changed
  console.log(isLoaded);
  const colorScheme = useColorScheme();

  console.log(colorScheme);

  if (isLoaded) {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar style="auto" />
      </SafeAreaProvider>
    );
  }
  return <Text>Loading...</Text>;
}
